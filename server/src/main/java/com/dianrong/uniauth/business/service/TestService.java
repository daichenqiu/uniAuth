package com.dianrong.uniauth.business.service;

import com.dianrong.uniauth.business.model.TestModel;
import com.dianrong.uniauth.data.entity.Test;
import com.dianrong.uniauth.data.repository.TestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestService {

    @Autowired
    private TestRepository testRep;

    public TestModel getTestById(Long id) {
        Test test = testRep.getTestById(id);
        return new TestModel()
            .setDegree(test.getDegree())
            .setId(test.getId())
            .setName(test.getName())
            .setSex(test.getSex());
    }
}

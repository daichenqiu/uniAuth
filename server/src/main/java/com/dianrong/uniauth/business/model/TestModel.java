package com.dianrong.uniauth.business.model;

public class TestModel {

    private Long id;
    private String name;
    private String sex;
    private Double degree;

    public Long getId() {
        return id;
    }

    public TestModel setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public TestModel setName(String name) {
        this.name = name;
        return this;
    }

    public String getSex() {
        return sex;
    }

    public TestModel setSex(String sex) {
        this.sex = sex;
        return this;
    }

    public Double getDegree() {
        return degree;
    }

    public TestModel setDegree(Double degree) {
        this.degree = degree;
        return this;
    }

}

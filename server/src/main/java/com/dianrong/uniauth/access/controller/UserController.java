package com.dianrong.uniauth.access.controller;

import com.dianrong.uniauth.access.support.result.JsonResult;
import com.dianrong.uniauth.business.model.TestModel;
import com.dianrong.uniauth.business.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import java.sql.*;

import static com.dianrong.uniauth.access.support.result.JsonResult.success;

/**
 * Created by guoying on 2015/9/2.
 */

@RestController
public class UserController {

    @Autowired
    private TestService testService;

    @RequestMapping(value = "/userAuth", method = RequestMethod.POST)
    public JsonResult userAuth(@RequestParam("domain") String domain,
                               @RequestParam("username") String username,
                               @RequestParam("password") String password,
                               HttpServletRequest request) {
        return success();
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public JsonResult test(@RequestParam("id") Long id) {
        TestModel testModel = testService.getTestById(id);
        return success(testModel);
    }

    @RequestMapping(value = "/userInfo", method = RequestMethod.GET)
    public JsonResult userInfo(@RequestParam("userId") String userId,
                               HttpServletRequest request) {
        Connection conn = null;
        StringBuilder sb = new StringBuilder();
        try {
            // The newInstance() call is a work around for some
            // broken Java implementations
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/local?" +
                "user=root&password=root");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from TEST");
            ResultSetMetaData m = rs.getMetaData();
            int columns = rs.getMetaData().getColumnCount();
            for(int i=1;i<=columns;i++)
            {
                System.out.print(m.getColumnName(i));
                sb.append(m.getColumnName(i));
                System.out.print("\t\t");
                sb.append("\t\t");
            }
            System.out.println();
            while(rs.next()) {
                for(int i=1; i <= columns; i++) {
                    String str = rs.getString(i);
                    System.out.print(str);
                    sb.append(str);
                    System.out.print("\t\t");
                    sb.append("\t\t");
                }
                System.out.println();
                sb.append("\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success(sb);
    }

}

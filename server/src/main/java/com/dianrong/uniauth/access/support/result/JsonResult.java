package com.dianrong.uniauth.access.support.result;

import com.dianrong.uniauth.access.support.result.JsonError.ErrorKey;
import org.springframework.validation.BindingResult;


public class JsonResult {
    public static final String SUCCESS = "success";
    public static final String LOGIN = "login";
    public static final String ERROR = "error";
    private static final String ERROR_BRIEF = "Internal Error";

    private final JsonMessage message = new JsonMessage();
    private String result;
    private Object content;

    private JsonResult(String result, Object content) {
        this.result = result;
        if (content == null) {
            this.setBrifMessage("No Content");
        } else {
            this.setBrifMessage("Successfully return data");
        }
        this.content = content;
    }

    /**
     * When login failed ,return this.
     *
     * @return login failed.
     */
    public static JsonResult login() {
        return new JsonResult(LOGIN, null);
    }

    /**
     * When operate success, return this.
     *
     * @return success
     */
    public static JsonResult success() {
        return success(null);
    }

    /**
     * When operate success, and need attach an result back.
     *
     * @param obj success result.
     * @return success with content
     */
    public static JsonResult success(Object obj) {
        return new JsonResult(SUCCESS, obj);
    }

    /**
     * when operate error, return this.
     *
     * @return error
     */
    public static JsonResult error() {
        return error(ERROR_BRIEF);
    }

    /**
     * when operate error and need a error message, return this.
     *
     * @param message Error Message
     * @return error
     */
    public static JsonResult error(String message) {
        JsonResult jr = new JsonResult(ERROR, null);
        jr.getMessage().setBrief(message);
        return jr;
    }

    public static JsonResultChecker check(BindingResult bindingResult) {
        return new JsonResultChecker(bindingResult);
    }

    public String getResult() {
        return result;
    }

    public JsonMessage getMessage() {
        return message;
    }

    public Object getContent() {
        return content;
    }

    /**
     * set brif message
     *
     * @param message Message
     * @return jsonResult itself.
     */
    public JsonResult setBrifMessage(String message) {
        this.getMessage().setBrief(message);
        return this;
    }

    /**
     * add error description.
     *
     * @param field field
     * @param error error
     * @return jsonResult itself.
     */
    public JsonResult addError(String field, ErrorKey error) {
        JsonError je = new JsonError();
        je.setField(field);
        je.setError(error);
        this.getMessage().getErrors().add(je);
        return this;
    }

    @Override
    public String toString() {
        return "JsonResult{" + "message=" + message + ", result='" + result + '\'' + ", content="
            + content + '}';
    }

    public static class JsonResultChecker {
        private final BindingResult bindingResult;

        public JsonResultChecker(BindingResult bindingResult) {
            this.bindingResult = bindingResult;
        }

/*        public JsonResult succeed(Supplier<Object> supplier) {
            if (bindingResult != null && bindingResult.hasErrors()) {
                JsonResult jr = JsonResult.error();
                bindingResult.getFieldErrors().forEach(
                    fe -> jr.addError(fe.getField(), ErrorKey.INVALID_ARGS));
                return jr;
            }
            return JsonResult.success(supplier != null ? supplier.get() : null);
        }*/
    }
}

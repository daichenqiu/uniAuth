package com.dianrong.uniauth.access.support.result;

import com.dianrong.uniauth.support.gson.EnumResource;

public class JsonError {
    private String field;
    private ErrorKey error;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public ErrorKey getError() {
        return error;
    }

    public void setError(ErrorKey error) {
        this.error = error;
    }

    @EnumResource(prefix = "json.result", resourceName = "i18n/error")
    public enum ErrorKey {
        INVALID_ARGS, NO_ENOUGH_ARGS, WRONG_FORMAT, NUM_TOO_LARGE, NUM_TOO_SMALL,
    }
}

package com.dianrong.uniauth.data.repository;

import com.dianrong.uniauth.data.entity.Test;
import com.dianrong.uniauth.data.mapper.TestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TestRepository {

    @Autowired
    private TestMapper testMapper;

    public Test getTestById(Long id){
        return testMapper.selectByPrimaryKey(id);
    }
}

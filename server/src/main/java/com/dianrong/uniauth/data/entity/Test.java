package com.dianrong.uniauth.data.entity;

public class Test {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TEST.id
     *
     * @mbggenerated Wed Sep 02 19:04:46 CST 2015
     */
    private Long id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TEST.name
     *
     * @mbggenerated Wed Sep 02 19:04:46 CST 2015
     */
    private String name;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TEST.sex
     *
     * @mbggenerated Wed Sep 02 19:04:46 CST 2015
     */
    private String sex;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TEST.degree
     *
     * @mbggenerated Wed Sep 02 19:04:46 CST 2015
     */
    private Double degree;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TEST.id
     *
     * @return the value of TEST.id
     *
     * @mbggenerated Wed Sep 02 19:04:46 CST 2015
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TEST.id
     *
     * @param id the value for TEST.id
     *
     * @mbggenerated Wed Sep 02 19:04:46 CST 2015
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TEST.name
     *
     * @return the value of TEST.name
     *
     * @mbggenerated Wed Sep 02 19:04:46 CST 2015
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TEST.name
     *
     * @param name the value for TEST.name
     *
     * @mbggenerated Wed Sep 02 19:04:46 CST 2015
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TEST.sex
     *
     * @return the value of TEST.sex
     *
     * @mbggenerated Wed Sep 02 19:04:46 CST 2015
     */
    public String getSex() {
        return sex;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TEST.sex
     *
     * @param sex the value for TEST.sex
     *
     * @mbggenerated Wed Sep 02 19:04:46 CST 2015
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TEST.degree
     *
     * @return the value of TEST.degree
     *
     * @mbggenerated Wed Sep 02 19:04:46 CST 2015
     */
    public Double getDegree() {
        return degree;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TEST.degree
     *
     * @param degree the value for TEST.degree
     *
     * @mbggenerated Wed Sep 02 19:04:46 CST 2015
     */
    public void setDegree(Double degree) {
        this.degree = degree;
    }
}
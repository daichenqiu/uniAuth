package com.dianrong.uniauth.support;

import com.google.common.base.Strings;

import javax.validation.constraints.NotNull;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Objects;

/**
 * @author daniel
 * @date 2015-05-21
 * @since 1.0
 */
public class XmlUtils {

    /**
     * Transfer Xml string to target result with specific type.
     *
     * @param xmlstring source xml string
     * @param clazz     target result class
     * @param <T>       target Obect type.
     * @return if xmlstring is empty or null then return null, otherwise will return transferred target result.
     * @throws IllegalArgumentException throw this exception when transfer failed.
     */
    public static <T> T fromXml(String xmlstring, @NotNull Class<T> clazz) throws IllegalArgumentException {
        if (Strings.isNullOrEmpty(xmlstring)) {
            return null;
        }
        Objects.requireNonNull(clazz, "target class cannot be null!");
        try {
            JAXBContext context = JAXBContext.newInstance(clazz);
            Unmarshaller un = context.createUnmarshaller();
            return clazz.cast(un.unmarshal(new StringReader(xmlstring)));
        } catch (Exception e) {
            throw new IllegalArgumentException(xmlstring, e);
        }
    }

    /**
     * Transfer Object src to XML string.
     *
     * @param src source result.
     * @return if src is null then return null, otherwise it will try to transfer result src.
     * @throws IllegalArgumentException throw this exception when transfer failed.
     */
    public static String toXml(Object src) throws IllegalArgumentException {
        if (src == null) {
            return null;
        }
        try {
            JAXBContext context = JAXBContext.newInstance(src.getClass());
            Marshaller ms = context.createMarshaller();
            StringWriter sw = new StringWriter();
            ms.marshal(src, sw);
            return sw.toString();
        } catch (Exception e) {
            throw new IllegalArgumentException(src.toString(), e);
        }
    }
}

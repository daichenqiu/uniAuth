package com.dianrong.uniauth.support.spring;

import com.dianrong.uniauth.support.gson.CrcGson;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.http.converter.json.GsonHttpMessageConverter;

/**
 * Created by daniel on 15-4-21.
 */
public class GsonConverter implements FactoryBean<GsonHttpMessageConverter> {
    private GsonHttpMessageConverter instance = new GsonHttpMessageConverter();

    {
        instance.setGson(CrcGson.getInstance().getGson());
    }

    @Override
    public GsonHttpMessageConverter getObject() throws Exception {
        return instance;
    }

    @Override
    public Class<?> getObjectType() {
        return GsonHttpMessageConverter.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}

package com.dianrong.uniauth.support.gson;

import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
/*import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
*/
import java.util.Date;
import java.util.Locale;

/**
 * @author daniel
 * @date 2015-05-20
 * @since 1.0
 */
public class CrcGson {
    private final Gson gson;
    private final Gson matrixGson;
    //private final EnumResources resource;

    private CrcGson() {
        //this.resource = new EnumResources();
        this.gson = createGson(FieldNamingPolicy.IDENTITY);
        this.matrixGson = createGson(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
    }

    public static CrcGson getInstance() {
        return CrcGsonHolder.INSTANCE;
    }

    private Gson createGson(FieldNamingPolicy policy) {
        return new GsonBuilder()
            .setFieldNamingPolicy(policy)
                //Register ISO-8601 extended local date format.
            //.registerTypeAdapter(LocalDate.class, new LocalDateTypeAdapter())
                //Register ISO-8601 extended local time format.
            //.registerTypeAdapter(LocalTime.class, new LocalTimeTypeAdapter())
                //Register ISO-8601 extended local datetime format.
            //.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeTypeAdapter())
                //Register ISO-8601 instant format. equal to Instance
            //.registerTypeAdapter(Date.class, new DateTypeAdapter())
                //Register ISO-8601 instant format.
            //.registerTypeAdapter(Instant.class, new InstantAdapter())
                //Register Byte
            //.registerTypeAdapter(byte[].class, new ByteArrayAdapter())
                // Auto getOperationById Label for enum.
            //.registerTypeAdapterFactory(new ToLabel(policy))
                // Keep null
            .serializeNulls()
            .create();
    }

/*    public EnumResources getResource() {
        return resource;
    }*/

    public Gson getGson() {
        return gson;
    }

    public Gson getMatrixGson() {
        return matrixGson;
    }


/*    @SuppressWarnings("rawtypes")
    public String getLabel(Locale locale, Enum value) {
        return this.resource.getLabel(value, locale);
    }
*/
    private static class CrcGsonHolder {
        private static CrcGson INSTANCE = new CrcGson();
    }

/*    private static class LocalDateTypeAdapter extends TypeAdapter<LocalDate> {

        @Override
        public void write(JsonWriter out, LocalDate value) throws IOException {
            out.value((value == null ? null
                : DateTimeFormatter.ISO_LOCAL_DATE.format(value)));
        }

        @Override
        public LocalDate read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            String date = in.nextString();
            if (Strings.isNullOrEmpty(date)) {
                return null;
            }
            return LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE);
        }
    }

    private static class DateTypeAdapter extends TypeAdapter<Date> {

        @Override
        public void write(JsonWriter out, Date value) throws IOException {
            out.value((value == null ? null
                : DateTimeFormatter.ISO_INSTANT.format(value.toInstant())));
        }

        @Override
        public Date read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            String date = in.nextString();
            if (Strings.isNullOrEmpty(date)) {
                return null;
            }
            return Date.from(Instant.parse(date));
        }
    }

    private static class InstantAdapter extends TypeAdapter<Instant> {
        @Override
        public void write(JsonWriter out, Instant value) throws IOException {
            out.value((value == null ? null
                : DateTimeFormatter.ISO_INSTANT.format(value)));
        }

        @Override
        public Instant read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            String date = in.nextString();
            if (Strings.isNullOrEmpty(date)) {
                return null;
            }
            return Instant.parse(date);
        }
    }

    private static class LocalTimeTypeAdapter extends TypeAdapter<LocalTime> {
        @Override
        public void write(JsonWriter out, LocalTime value) throws IOException {
            out.value((value == null ? null
                : DateTimeFormatter.ISO_LOCAL_TIME.format(value)));
        }

        @Override
        public LocalTime read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            String date = in.nextString();
            if (Strings.isNullOrEmpty(date)) {
                return null;
            }
            return LocalTime.parse(date, DateTimeFormatter.ISO_LOCAL_TIME);
        }
    }

    private static class LocalDateTimeTypeAdapter extends TypeAdapter<LocalDateTime> {
        @Override
        public void write(JsonWriter out, LocalDateTime value) throws IOException {
            out.value((value == null ? null
                : DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(value)));
        }

        @Override
        public LocalDateTime read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            String date = in.nextString();
            if (Strings.isNullOrEmpty(date)) {
                return null;
            }
            return LocalDateTime.parse(date, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        }
    }

    private class ByteArrayAdapter extends TypeAdapter<byte[]> {
        @Override
        public void write(JsonWriter out, byte[] value) throws IOException {
            out.value((value == null ? null
                : new String(Base64.getEncoder().withoutPadding().encode(value), Charsets.UTF_8)));
        }

        @Override
        public byte[] read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            String data = in.nextString();
            if (Strings.isNullOrEmpty(data)) {
                return null;
            }
            return Base64.getDecoder().decode(data);
        }
    }*/
}

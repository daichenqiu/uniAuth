package com.dianrong.uniauth.support;

import ch.qos.logback.classic.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static java.util.Objects.requireNonNull;

/**
 * Created by jeffrey on 15-5-21.
 */
public class LogbackUtils {

    private static Logger logger = LoggerFactory.getLogger(LogbackUtils.class);

    /**
     * @param loggerName loggerName, not null or Empty.
     * @param level      OFF, ERROR, WARN, INFO, DEBUG, TRACE, ALL.
     */
    public static boolean setLoggerLevel(String loggerName, String level) {
        try {
            return setLoggerLevel(LoggerFactory.getLogger(loggerName),
                (Level) Level.class.getField(level).get(null));
        } catch (IllegalAccessException | NoSuchFieldException e) {
            logger.error("set logger level error, loggerName:{}, level:{}", loggerName, level, e);
            return false;
        }
    }

    /**
     * @param logger logger.
     * @param level  OFF, ERROR, WARN, INFO, DEBUG, TRACE, ALL.
     */
    public static boolean setLoggerLevel(Logger logger, String level) {
        try {
            return setLoggerLevel(logger, (Level) Level.class.getField(level).get(null));
        } catch (IllegalAccessException | NoSuchFieldException e) {
            logger.error("set logger level error, logger:{}, level:{}", logger, level, e);
            return false;
        }
    }

    public static boolean setLoggerLevel(Logger logger, Level level) {
        ((ch.qos.logback.classic.Logger) requireNonNull(logger)).setLevel(requireNonNull(level));
        return true;
    }

}

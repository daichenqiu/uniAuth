package com.dianrong.uniauth.support;

import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

/**
 * Created by 晟龙 on 2015/9/7.
 */
public class MyWebAppInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext servletContext) {

        // 1. init application context first
        XmlWebApplicationContext appContext = new XmlWebApplicationContext();
        appContext.setServletContext(servletContext);
        appContext.setConfigLocation("classpath:spring-context.xml");
        appContext.refresh();

        // 2. Add SpringMVC servlet and init it with appContext
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("dispatcher", new DispatcherServlet(appContext));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/v1/*");

        // 3. Add CXF servlet and init it with appContext
        servletContext.setAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE, appContext);
        ServletRegistration.Dynamic cxfDispatcher = servletContext.addServlet("CXFServlet", new CXFServlet());
        cxfDispatcher.setLoadOnStartup(1);
        cxfDispatcher.addMapping("/ws/*");

    }

}

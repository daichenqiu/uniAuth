package com.dianrong.uniauth.support.gson;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by daniel on 15-4-20.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface EnumResource {
    /**
     * prefix of this Enum in the resource file.
     *
     * @return prefix
     */
    String prefix() default "";

    /**
     * resource name of the i18n message. Default use the name of type.
     *
     * @return resource name
     */
    String resourceName() default "";
}

package com.dianrong.uniauth.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dianrong.uniauth.business.model.TestModel;
import com.dianrong.uniauth.business.service.TestService;

/**
 * Created by ���� on 2015/9/7.
 */
@Service
@Path("/")
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class TestResource {

    @Autowired
    private TestService testService;

    @GET
    @Path("/test")
    public TestModel get(@QueryParam("id") Long id) {
        TestModel model = testService.getTestById(id);
        return model;
    }

//    @POST
//    @Path("/test")
//    public Result update(TestModel testModel) {
//        Boolean b = testService.updateTest(testModel);
//        return b ? success(): error();
//    }
//
//    @PUT
//    @Path("/test")
//    public Result save(TestModel testModel) {
//        return testService.insertTest(testModel) ? success(): error();
//    }
//
//    @DELETE
//    @Path("/test")
//    public Result delete() {
//        //TestModel model = testService.getTestById(1L);
//        return success();
//    }
}
import java.sql.*;

public class TestJDBCConnect {
    public static void main(String[] args) {
        Connection conn = null;
        try {
            // The newInstance() call is a work around for some
            // broken Java implementations

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/local?" +
                    "user=root&password=root");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from TEST");
            ResultSetMetaData m = rs.getMetaData();
            int columns = rs.getMetaData().getColumnCount();
            for(int i=1;i<=columns;i++)
            {
                System.out.print(m.getColumnName(i));
                System.out.print("\t\t");
            }
            System.out.println();
            while(rs.next()) {
                for(int i=1; i <= columns; i++) {
                    System.out.print(rs.getString(i));
                    System.out.print("\t\t");
                }
                System.out.println();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package com.dianrong.uniauth.data;

/**
 * Created by Guoying on 2015/9/6.
 */
public enum UserGroup {
    PLATINUM,
    GOLD,
    SLIVER;
}

package com.dianrong.uniauth.data;

import java.util.List;

/**
 * Created by Guoying on 2015/9/6.
 */
public class UserMenu {

    private String name;

    private boolean isDropable;

    // relative path, can be empty
    private String url;

    private List<UserMenu> subMenus;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDropable() {
        return isDropable;
    }

    public void setIsDropable(boolean isDropable) {
        this.isDropable = isDropable;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<UserMenu> getSubMenus() {
        return subMenus;
    }

    public void setSubMenus(List<UserMenu> subMenus) {
        this.subMenus = subMenus;
    }
}
